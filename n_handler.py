#!/usr/bin/python

import sys
import smtplib
import MySQLdb
import handlers 
import time

#alerts db name
alerts_db = "alerts"
# Validate MySQL connection
conn = MySQLdb.connect ( host = "localhost", user = "root", passwd = "flash109", db = alerts_db)
cursor = conn.cursor ()

# globals
# Grab current time to use for current timestamp
now = time.strftime('%Y-%m-%d %H:%M:%S')
disable_email = 0
from_address = "noc@sourceallies.com"
n_state = sys.argv[1]
n_service = sys.argv[2]
n_host = sys.argv[3]
n_host_ip = sys.argv[4]
n_service_output = sys.argv[5]
n_default_email = sys.argv[6]
n_attempt = sys.argv[7]
n_type = sys.argv[8]

# Only act on alerts that are not part of a scheduled downtime (DOWNTIMESTART)
if n_type != "DOWNTIMESTART":
	if disable_email > 0:
	        handlers.emailAlert(n_service, n_host, n_state, n_service_output)

## If new then insert into  Alerts Database if its not a re-notification, ack, or recover. No need to create another alert for a Flapping event
	if int(n_attempt) == 1 and n_type != "RECOVERY" and n_type != "ACKNOWLEDGEMENT" and n_type != "FLAPPING":
		sql = "INSERT INTO alerts (host,ipaddress,service,createdate,acknowledged,recovered,notes) \
	    	      VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" % \
	      	      (n_host, n_host_ip, n_service, now, '0','0','')		
		cursor.execute(sql)
## If a RECOVERY event then update the appropriate record in the alert DB"
	if n_type == "RECOVERY":
		sql = "UPDATE alerts SET recovered=1 WHERE service = "+n_service+" AND host = "+n_host
		cursor.execute(sql) 

## Finally clean up
cursor.close ()
conn.close ()
exit(0)
