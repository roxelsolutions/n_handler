import smtplib
from email.mime.text import MIMEText

def emailAlert(service, host, state, service_output):
	from_address = "noc@sourceallies.com"
        msg = MIMEText("***** Icinga *****\n\nService: " + service + "\nHost: " + host + "\nState: " + state + "\n\nAdditional Info:\n\n" + service_output + "\n\nhttp://noc.appcore.com/portal/event")
        msg['Subject'] = "NOC AlErT!"
        msg["From"] = from_address
        msg["To"] = "bhaase@sourceallies.com"
        s = smtplib.SMTP('localhost')
        s.sendmail(from_address, ["bhaase@sourceallies.com"], msg.as_string())
        s.quit()

