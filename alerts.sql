CREATE TABLE `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(20) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `acknowledged` int(11) DEFAULT NULL,
  `notes` text,
  `who` varchar(50) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `ackdate` datetime DEFAULT NULL,
  `recovered` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2190 DEFAULT CHARSET=latin1;

