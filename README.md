n_handler
=========

Icinga / Nagios event handler.  Allows for multiple actions to be taken on events.  Such as send emails, log to an alert DB, or open a JIRA ticket
