// Create global mission control scope object
MC = {};

// wire up common ajax events
$(function(){
        $("[data-async]=true").on('click', function(event){
                $.ajax({
                        url: $(this).attr('href'),
                        success: function(data){}
                })
                event.preventDefault();
        })

        //setup elements with gauges
        $("[data-gauge]").each(function(){
                var gauge = $(this).data('gauge');
                if( gauge && gauge.value >= 0 ){
                        gauge.id = $(this).attr('id');
                        var g = new JustGage( gauge );
                }
        });
});

