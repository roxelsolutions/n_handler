# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext, loader

from console.models import Alerts

def index(request):
    open_issue_list = Alerts.objects.filter(acknowledged=0)
    handled_issue_list = Alerts.objects.filter(acknowledged=1)
    template = loader.get_template('console/base_console.html')
    context = RequestContext(request, {
        'open_issue_list': open_issue_list,
	'handled_issue_list': handled_issue_list,
    })
    return HttpResponse(template.render(context))

def ajax_vote(request, alert_id, notes):
    my_alert = Alerts.objects.filter(id=alert_id)
    if my_alert:
        my_alert = my_alert[0]
        my_alert.acknowledged = "1"
	my_alert.notes = notes
        my_alert.save()
	return HttpResponseRedirect('/console')
