from django.conf.urls import patterns, url

from console import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<alert_id>\d+)/(?P<notes>.*)/ajaxvote/$', 'console.views.ajax_vote'),    
)
