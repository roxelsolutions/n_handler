from django.db import models

# Create your models here.
class Alerts(models.Model):
    host = models.CharField(max_length=200)
    ipaddress = models.CharField(max_length=200)
    service = models.CharField(max_length=200)
    acknowledged = models.IntegerField()
    notes = models.CharField(max_length=200)
    who = models.CharField(max_length=200)
    createdate = models.DateTimeField('date created')
    ackdate = models.DateTimeField('date ack')
    recovered = models.IntegerField()

def get_absolute_url(self):
    return u"/console/%s/" % self.pk
